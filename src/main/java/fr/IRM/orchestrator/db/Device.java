package fr.IRM.orchestrator.db;

import java.util.ArrayList;

public class Device {

	private String name;
	private int value;
	private ArrayList<Integer> elderValues;
	
	public Device() {
		super();
	}
	
	public Device(String name, int value, ArrayList<Integer> elderValues) {
		super();
		this.name = name;
		this.value = value;
		this.elderValues = elderValues;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public int getValue() {
		return value;
	}

	public void setValue(int value) {
		this.value = value;
	}

	public ArrayList<Integer> getElderValues() {
		return elderValues;
	}

	public void setElderValues(ArrayList<Integer> elderValues) {
		this.elderValues = elderValues;
	}
	
	public void addElderValue(int newValue) {
		this.elderValues.add(newValue);
	}
}
