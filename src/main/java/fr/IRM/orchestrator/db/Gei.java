package fr.IRM.orchestrator.db;

import java.util.ArrayList;

// Class used to mirror the OM2M architecture and to simulate DataBase
public class Gei {
	
	// Instance of Singleton
	private static Gei INSTANCE = null;
	private ArrayList<Room> rooms;
	private ArrayList<Device> externalSensors;
	
	// Constructor of Singleton
	private Gei() {}
	
	// Instance getter
	public static synchronized Gei getInstance() {
		if(INSTANCE == null) {
			INSTANCE = new Gei();
		}
		return INSTANCE;
	}

	public ArrayList<Room> getRooms() {
		return this.rooms;
	}

	public void setRooms(ArrayList<Room> rooms) {
		this.rooms = rooms;
	}
	
	public void addRoom(Room newRoom) {
		this.rooms.add(newRoom);
	}
	
	public ArrayList<Device> getExternalSensors() {
		return this.externalSensors;
	}

	public void setExternalSensors(ArrayList<Device> externalSensors) {
		this.externalSensors = externalSensors;
	}
	
	public void addExternalSensor(Device newExternalSensor) {
		this.externalSensors.add(newExternalSensor);
	}
}
