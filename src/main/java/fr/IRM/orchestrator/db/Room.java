package fr.IRM.orchestrator.db;

import java.util.ArrayList;

public class Room {
	
	private String name;
	private String id;
	private ArrayList<Device> devices = new ArrayList<Device>();

	public Room() {
		super();
	}
	
	public Room(String name, String id) {
		super();
		this.name = name;
		this.id = id;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getId() {
		return this.id;
	}

	public void setId(String id) {
		this.id = id;
	}

	public ArrayList<Device> getDevices() {
		return devices;
	}

	public void setDevices(ArrayList<Device> devices) {
		this.devices = devices;
	}
	
	public void addDevice(Device newDevice) {
		this.devices.add(newDevice);
	}
}
