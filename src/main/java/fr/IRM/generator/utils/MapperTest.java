package fr.IRM.generator.utils;

import org.eclipse.om2m.commons.mapper.Mapper;
import org.eclipse.om2m.commons.mapper.MapperInterface;
import org.eclipse.om2m.commons.resource.AE;

public class MapperTest {

	public static void main(String[] args) {
		MapperInterface mapper = new Mapper();

		// example to test marshal operation
		AE ae = new AE();
		ae.setName("test");
		ae.setRequestReachability(false); 
		String aeMarshalled = mapper.marshal(ae);
		System.out.println(aeMarshalled);

		Object aeUnmarshalled = new AE();
		aeUnmarshalled = mapper.unmarshal(aeMarshalled);
		
		// TODO test unmarshal
		// get the XML representation, parse it with unmarshal operation
	}
	
}
