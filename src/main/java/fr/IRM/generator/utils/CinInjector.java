package fr.IRM.generator.utils;

import java.io.IOException;
import java.util.List;
import java.util.Random;

import org.eclipse.om2m.commons.client.Client;
import org.eclipse.om2m.commons.client.Response;

public class CinInjector extends Thread {

	private String roomName;
	// private List<String> aeList;
	private String cntEndpoint;
	private String aeEndpoint;
	private String originator;
	private String url;
	private Client client;
	private CreateUtils createUtils = new CreateUtils();
	Random rand = new Random();
	Response response = new Response();

	public CinInjector() {
		super();
	}

	public CinInjector(String roomName, String cntEndpoint, String aeEndpoint,
			String originator, String url, Client client) {
		super();
		this.roomName = roomName;
		this.cntEndpoint = cntEndpoint;
		this.aeEndpoint = aeEndpoint;
		this.originator = originator;
		this.url = url;
		this.client = client;
	}

	private static void threadPrint(String s, String roomName) {
		System.out.println(roomName + " : " + s);
	}

	@Override
	public void run() {
		while (true) {
			try {
				int randValue = rand.nextInt(40);
				response = createUtils.createCIN(createUtils.generateDataCIN(aeEndpoint,
						roomName, randValue), url + aeEndpoint + "/"
						+ cntEndpoint, originator, client);
				threadPrint(
						"updated " + aeEndpoint + " with value : "
								+ Integer.toString(randValue), roomName);
				//response.printResponse();
				Thread.sleep(15000);
			} catch (InterruptedException e) {
				e.printStackTrace();
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
	}

}
