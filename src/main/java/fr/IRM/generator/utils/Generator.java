package fr.IRM.generator.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;
import java.util.Scanner;

import org.eclipse.om2m.commons.client.Client;
import org.eclipse.om2m.commons.client.Response;
import org.eclipse.om2m.commons.mapper.Mapper;
import org.eclipse.om2m.commons.mapper.MapperInterface;
import org.eclipse.om2m.commons.resource.AE;
import org.eclipse.om2m.commons.resource.Container;
import org.eclipse.om2m.commons.resource.ContentInstance;
import org.eclipse.om2m.commons.util.RequestLoader;

import obix.Bool;
import obix.Int;
import obix.Obj;
import obix.Str;
import obix.io.ObixDecoder;
import obix.io.ObixEncoder;

public class Generator {
	
	public static void main(String[] args) {	
		Client client = new Client();
		Response response = new Response();
		Injector injector = new Injector();

		final String baseUrl = "http://localhost:8080/~/";
		final String originator = "admin:admin";
		
		List<String> roomAEList = Arrays.asList("window", "door", "light", "presence", "temp", "heater");
		List<String> externalAEList = Arrays.asList("externalTemp", "externalPres");
		List<String> aeCNTList = Arrays.asList("descriptor", "data");
		
		Scanner scanner = new Scanner(System.in);
		String i;
		
		do {
			System.out.println("----IRM---- OM2M architecture generator ----IRM----");
			System.out.println("Generate all architecture ? (o/n)");
			i = scanner.next();
		} while ((!i.equals("o")) && (!i.equals("n")));

		if(i.equals("o")) {
			injector.inject(externalAEList, aeCNTList, baseUrl + "in-cse/in-gei/", "in-gei", client, response, originator);
			injector.inject(roomAEList, aeCNTList, baseUrl + "mn-room_01/mn-room_01/", "mn-room_01", client, response, originator);
			injector.inject(roomAEList, aeCNTList, baseUrl + "mn-room_02/mn-room_02/", "mn-room_02", client, response, originator);
		}
		
		CinInjector room01 = new CinInjector("mn-room_01", "data", "temp", originator, baseUrl + "mn-room_01/mn-room_01/", client);
		room01.start();
	}
}