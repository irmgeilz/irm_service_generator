package fr.IRM.generator.utils;

import java.io.IOException;
import java.math.BigInteger;
import java.util.Random;

import obix.Int;
import obix.Obj;
import obix.Str;
import obix.io.ObixEncoder;

import org.eclipse.om2m.commons.client.Client;
import org.eclipse.om2m.commons.client.Response;
import org.eclipse.om2m.commons.mapper.Mapper;
import org.eclipse.om2m.commons.resource.AE;
import org.eclipse.om2m.commons.resource.Container;
import org.eclipse.om2m.commons.resource.ContentInstance;
import org.eclipse.om2m.commons.resource.Subscription;

public class CreateUtils {
	
	public Response createAE(String name, String appId, Boolean rr,
			String url, String originator, Client client) throws IOException {
		Response response = new Response();
		Mapper mapper = new Mapper();

		AE ae = new AE();
		ae.setName(name);
		ae.setAppID(appId);
		ae.setRequestReachability(rr);

		response = client.create(url, mapper.marshal(ae), originator, "2");
		return response;
	}
	
	public Response createMonitorSUB(String url, String originator, Client client, String monitorUrl) throws IOException {
		Response response = new Response();
		Mapper mapper = new Mapper();		
		
		Subscription sub = new Subscription();
		sub.setName("sub_orchestrator");
		sub.getNotificationURI().add(monitorUrl);
		sub.setNotificationContentType(BigInteger.ONE);
		
		response = client.create(url, mapper.marshal(sub), originator, "23");
		return response;
	}

	public Response createCNT(String name, String url,
			String originator, Client client) throws IOException {
		Response response = new Response();
		Mapper mapper = new Mapper();

		Container cnt = new Container();
		cnt.setName(name);

		response = client.create(url, mapper.marshal(cnt), originator, "3");
		return response;
	}

	public Response createCIN(String content, String url,
		String originator, Client client) throws IOException {
		Response response = new Response();
		Mapper mapper = new Mapper();

		ContentInstance cin = new ContentInstance();
		cin.setContent(content);

		response = client.create(url, mapper.marshal(cin), originator, "4");
		return response;
	}

	public String generateDescriptor(String aeName, String roomID) {
		Str type = new Str(aeName);
		Str room = new Str(roomID);
		Obj object = new Obj();
		object.add("Type", type);
		object.add("RoomId", room);

		return ObixEncoder.toString(object);
	}
	
	public String generateDataCIN(String typeAe, String roomId, int cinValue) {
		Str type = new Str(typeAe);
		Str room = new Str(roomId);
		Int value = new Int(cinValue);
		Obj object = new Obj();
		object.add("Type", type);
		object.add("RoomId", room);
		object.add("Value", value);

		return ObixEncoder.toString(object);
	}
}
