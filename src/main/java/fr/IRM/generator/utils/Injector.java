package fr.IRM.generator.utils;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.eclipse.om2m.commons.client.Client;
import org.eclipse.om2m.commons.client.Response;

import fr.IRM.orchestrator.db.Device;
import fr.IRM.orchestrator.db.Room;

public class Injector {
	
	private static CreateUtils createUtils = new CreateUtils();
	
	private static int defaultValue = 0;
	
	private static void printError(int code) {
		System.out.println("\n--!An error occured during generation !--");
		if (code == 404)
			System.out.println("Error : 404 Could not find endpoint");
		if (code == 409)
			System.out
					.println("Error : 409 A ressources with the same name are already created");
	}

	public Response inject(List<String> aeList, List<String> cntList, String url, String endPoint, Client client, Response response, String originator) {
		try {
			System.out.println("Generating ressources on : " + url);
			System.out.print("Generation of ressources ...");
			for (String ae : aeList) {
				response = createUtils.createAE(ae, "app-sensor", false, url,
						originator, client);
				if (response.getStatusCode() != 201 && response.getStatusCode() != 409) {
					printError(response.getStatusCode());
					return response;
				}
				System.out.print(".");
				for (String cnt : cntList) {
					response = createUtils.createCNT(cnt, url + ae, originator, client);
					switch(cnt) {
						case "descriptor": 
							response = createUtils.createCIN(createUtils.generateDescriptor(ae, endPoint), url + ae + "/" + cnt, originator, client);
							break;
						case "data":
							response = createUtils.createMonitorSUB(url + ae + "/" + cnt, originator, client, "http://localhost:7000/monitor");
							response = createUtils.createCIN(createUtils.generateDataCIN(ae, endPoint, defaultValue), url + ae + "/" + cnt, originator, client);
							break;
					}
					if (response.getStatusCode() != 201 && response.getStatusCode() != 409) {
						printError(response.getStatusCode());
						return response;
					}
					System.out.print(".");
				}
			}
		} catch (IOException e) {
			e.printStackTrace();
		}

		System.out.println("\nDone");
		return response;
	}
	
	public Room createRoomObject(List<String> aeList, String roomName) {
		Room room = new Room();
		room.setName(roomName);
		room.setId(roomName);
		for(String s : aeList) {
			Device d = new Device();
			d.setName(s);
			d.setValue(defaultValue);
			room.addDevice(d);
		}
		return room;
	}
	
	public ArrayList<Device> createExternalSensorsObject(List<String> aeList) {
		ArrayList<Device> externalSensorsList = new ArrayList<Device>();
		for(String s : aeList) {
			Device d = new Device();
			d.setName(s);
			d.setValue(defaultValue);
			externalSensorsList.add(d);
		}
		return externalSensorsList;
	}
}
