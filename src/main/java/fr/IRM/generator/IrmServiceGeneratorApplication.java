package fr.IRM.generator;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.scheduling.annotation.EnableScheduling;

@SpringBootApplication
@EnableScheduling
public class IrmServiceGeneratorApplication {

	public static void main(String[] args) {
		SpringApplication.run(IrmServiceGeneratorApplication.class, args);
	}

}
