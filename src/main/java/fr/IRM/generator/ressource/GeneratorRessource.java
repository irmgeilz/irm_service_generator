package fr.IRM.generator.ressource;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Random;

import org.eclipse.om2m.commons.client.Client;
import org.eclipse.om2m.commons.client.Response;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RestController;

import fr.IRM.generator.utils.CinInjector;
import fr.IRM.generator.utils.CreateUtils;
import fr.IRM.generator.utils.Injector;
import fr.IRM.orchestrator.db.Device;
import fr.IRM.orchestrator.db.Room;

@RestController
public class GeneratorRessource {
	private Client client = new Client();
	private Response response = new Response();
	private Injector injector = new Injector();
	private CreateUtils createUtils = new CreateUtils();
	
	private Random rand = new Random();
	
	private final String baseUrl = "http://localhost:8080/~/";
	private final String originator = "admin:admin";
	
	private List<String> roomAEList = Arrays.asList("window", "door", "light", "presence", "temp", "heater");
	private List<String> externalAEList = Arrays.asList("externalTemp", "externalPres", "alarm");
	private List<String> aeCNTList = Arrays.asList("descriptor", "data");
	
	private ArrayList<String> roomsCreatedList = new ArrayList<String>();
	private boolean externalCreated = false;
	
	@Scheduled(fixedRate=15000)
	public void randomValueGenerator() {
		for(String room : roomsCreatedList) {
			try {
				response = createUtils.createCIN(createUtils.generateDataCIN("temp", room, rand.nextInt(40)), baseUrl + room + "/" + room + "/" + "temp" + "/"+ "data", originator, client);
			} catch (IOException e) {
				System.out.println("Error during scheduled data generation of room : " + room + " ...");
				e.printStackTrace();
			}
		}
		if(externalCreated) {
			try {
				response = createUtils.createCIN(createUtils.generateDataCIN("externalTemp", "in-gei", rand.nextInt(40)), baseUrl + "in-cse" + "/" + "in-gei" + "/" + "externalTemp" + "/"+ "data", originator, client);
			} catch (IOException e) {
				System.out.println("Error during scheduled data generation of externalTemp ...");
				e.printStackTrace();
			}
		}
	}
	
	@GetMapping("/generate/{room}")
	public Response generateRoom(@PathVariable String room) {
		Response resp = new Response();
		//Room r;
		resp = injector.inject(roomAEList, aeCNTList, baseUrl + room + "/" + room + "/", room, client, response, originator);
		System.out.println("Room generated with code : " + resp.getStatusCode()/* + " message : " + resp.getRepresentation()*/);
		//r = injector.createRoomObject(roomAEList, room);
		if((resp.getStatusCode() == 201) || (resp.getStatusCode() == 409)) {
			System.out.println("Adding " + room + " to roomsCreatedList ...");
			roomsCreatedList.add(room);
		}
		return resp;		
	}
	
	@GetMapping("/generate/external")
	public Response generateExternal() {
		Response resp = new Response();
		//ArrayList<Device> d;
		resp = injector.inject(externalAEList, aeCNTList, baseUrl + "in-cse/in-gei/", "in-gei", client, response, originator);	
		System.out.println("External sensors generated with code : " + resp.getStatusCode());
		//d = injector.createExternalSensorsObject(externalAEList);
		if((resp.getStatusCode() == 201) || (resp.getStatusCode() == 409)) externalCreated = true;
		return resp;
	}
}
