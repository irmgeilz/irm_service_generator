package org.eclipse.om2m.commons.client;

/**
 * Class to store the response from a request
 *
 */
public class Response {
	String representation;
	int statusCode;

	public Response() {
		representation = "";
		statusCode = 0;
	}

	public Response(String rep, int status) {
		this.representation = rep;
		this.statusCode = status;
	}

	public String getRepresentation() {
		return representation;
	}

	public void setRepresentation(String representation) {
		this.representation = representation;
	}

	public int getStatusCode() {
		return statusCode;
	}

	public void setStatusCode(int statusCode) {
		this.statusCode = statusCode;
	}

	@Override
	public String toString() {
		return "Response [representation=" + representation + ", statusCode="
				+ statusCode + "]";
	}
	
	public void printResponse() {
		System.out.printf("Response to request : status %s - representation : %s%n", this.getStatusCode(), this.getRepresentation());
	}
}
